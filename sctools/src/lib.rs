use anyhow::Result;
use log::info;
use std::time::Duration;

pub fn reset_card(reset_pin: u32) -> Result<()> {
    info!("resetting concentrator micro...");
    let reset_pin = sysfs_gpio::Pin::new(reset_pin.into());
    reset_pin.with_exported(|| {
        // if you call set_direction right away, it crashes
        std::thread::sleep(Duration::from_millis(100));

        reset_pin.set_direction(sysfs_gpio::Direction::Out)?;
        std::thread::sleep(Duration::from_millis(100));
        reset_pin.set_value(0)?;
        std::thread::sleep(Duration::from_millis(100));
        reset_pin.set_value(1)?;
        Ok(())
    })?;

    Ok(())
}
