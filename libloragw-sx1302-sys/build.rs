extern crate bindgen;

use std::env;
use std::path::PathBuf;

fn main() {
    // Tell cargo to tell rustc to link the system bzip2
    // shared library.
    println!("cargo:rustc-link-lib=loragw");

    // Tell cargo to invalidate the built crate whenever the wrapper changes
    println!("cargo:rerun-if-changed=wrapper.h");

    // The bindgen::Builder is the main entry point
    // to bindgen, and lets you build up options for
    // the resulting bindings.
    let bindings = bindgen::Builder::default()
        // The input header we would like to generate
        // bindings for.
        .header("wrapper.h")
        .clang_arg("-Ivendor/sx1302_hal_cfg")
        .clang_arg("-Ivendor/sx1302_hal/libloragw/inc")
        // Tell cargo to invalidate the built crate whenever any of the
        // included header files changed.
        .parse_callbacks(Box::new(bindgen::CargoCallbacks))
        // Finish the builder and generate the bindings.
        .generate()
        // Unwrap the Result and panic on failure.
        .expect("Unable to generate bindings");

    // Write the bindings to the $OUT_DIR/bindings.rs file.
    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
    bindings
        .write_to_file(out_path.join("bindings.rs"))
        .expect("Couldn't write bindings!");

    // Build `libtinymt32` (mersenne twister) which `libloragw` depends on.
    cc::Build::new()
        .include("vendor/sx1302_hal/libtools/inc")
        .file("vendor/sx1302_hal/libtools/src/tinymt32.c")
        .static_flag(true)
        .compile("tinymt32");

    // Build our extracted, modified, and vendored `libloragw`.
    cc::Build::new()
        .include("vendor/sx1302_hal/libloragw/inc")
        .include("vendor/sx1302_hal/libtools/inc")
        .include("vendor/sx1302_hal_cfg")
        .file("vendor/sx1302_hal/libloragw/src/loragw_ad5338r.c")
        .file("vendor/sx1302_hal/libloragw/src/loragw_aux.c")
        .file("vendor/sx1302_hal/libloragw/src/loragw_cal.c")
        .file("vendor/sx1302_hal/libloragw/src/loragw_com.c")
        .file("vendor/sx1302_hal/libloragw/src/loragw_debug.c")
        .file("vendor/sx1302_hal/libloragw/src/loragw_gps.c")
        .file("vendor/sx1302_hal/libloragw/src/loragw_hal.c")
        .file("vendor/sx1302_hal/libloragw/src/loragw_i2c.c")
        .file("vendor/sx1302_hal/libloragw/src/loragw_lbt.c")
        .file("vendor/sx1302_hal/libloragw/src/loragw_mcu.c")
        .file("vendor/sx1302_hal/libloragw/src/loragw_reg.c")
        .file("vendor/sx1302_hal/libloragw/src/loragw_spi.c")
        .file("vendor/sx1302_hal/libloragw/src/loragw_stts751.c")
        .file("vendor/sx1302_hal/libloragw/src/loragw_sx125x.c")
        .file("vendor/sx1302_hal/libloragw/src/loragw_sx1250.c")
        .file("vendor/sx1302_hal/libloragw/src/loragw_sx1261.c")
        .file("vendor/sx1302_hal/libloragw/src/loragw_sx1302_rx.c")
        .file("vendor/sx1302_hal/libloragw/src/loragw_sx1302_timestamp.c")
        .file("vendor/sx1302_hal/libloragw/src/loragw_sx1302.c")
        .file("vendor/sx1302_hal/libloragw/src/loragw_usb.c")
        .file("vendor/sx1302_hal/libloragw/src/sx125x_com.c")
        .file("vendor/sx1302_hal/libloragw/src/sx125x_spi.c")
        .file("vendor/sx1302_hal/libloragw/src/sx1250_com.c")
        .file("vendor/sx1302_hal/libloragw/src/sx1250_spi.c")
        .file("vendor/sx1302_hal/libloragw/src/sx1250_usb.c")
        .file("vendor/sx1302_hal/libloragw/src/sx1261_com.c")
        .file("vendor/sx1302_hal/libloragw/src/sx1261_spi.c")
        .file("vendor/sx1302_hal/libloragw/src/sx1261_usb.c")
        .static_flag(true)
        .compile("loragw");
}
