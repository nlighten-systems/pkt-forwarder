#/bin/bash

set -o errexit
set -o nounset
set -o pipefail
set -o xtrace

readonly TARGET_HOST=paul@192.168.2.16
readonly TARGET_PATH=/home/paul/
readonly TARGET_ARCH=armv7-unknown-linux-gnueabihf
readonly SOURCE_PATH=target/${TARGET_ARCH}/debug/pktfwd

RUSTFLAGS='-C target-feature=+crt-static' BINDGEN_EXTRA_CLANG_ARGS='--sysroot /usr/lib/arm-linux-gnueabihf' cargo build --bin=pktfwd --bin=toyserver --target=${TARGET_ARCH}
rsync --progress --partial ${SOURCE_PATH} ${TARGET_HOST}:${TARGET_PATH}
rsync --progress --partial target/${TARGET_ARCH}/debug/toyserver ${TARGET_HOST}:${TARGET_PATH}
#ssh -t ${TARGET_HOST} ${TARGET_PATH}