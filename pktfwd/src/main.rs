mod base64_serde;
mod gwmp;

use anyhow::{Context, Result};
use base64::prelude::*;
use cfg::RootConfig;
use clap::Parser;
use futures::prelude::*;
use gwmp::types::PullResp;
use loragw_hw::lib::*;
use std::{convert::TryInto, fs::File, ops::Deref, path::PathBuf};
use tokio::signal::unix::{signal, SignalKind};

use crate::gwmp::GWMPClient;

#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Cli {
    #[clap(short, long, parse(from_os_str), value_name = "FILE")]
    config: PathBuf,

    #[clap(short, long, default_value_t = false)]
    verbose: bool,
}

fn reset(reset_pin: u32) {
    use std::time::Duration;

    info!("resetting concentrator micro...");
    let reset_pin = sysfs_gpio::Pin::new(reset_pin.into());
    if let Err(e) = reset_pin.with_exported(|| {
        std::thread::sleep(Duration::from_millis(100));
        reset_pin.set_direction(sysfs_gpio::Direction::Out)?;
        std::thread::sleep(Duration::from_millis(100));
        reset_pin.set_value(0)?;
        std::thread::sleep(Duration::from_millis(100));
        reset_pin.set_value(1)?;
        Ok(())
    }) {
        error!("error resetting micro: {}", e);
    }

    info!("waiting for micro to boot...");
    //std::thread::sleep(Duration::from_secs(1));
    std::thread::sleep(Duration::from_secs(20));
}

fn init_logging() {
    //use simple_logger::SimpleLogger;
    //SimpleLogger::new().init().unwrap();

    env_logger::init();
    // env_logger::builder()
    //     .filter_level(log::LevelFilter::Info)
    //     .init();
}

fn to_tx(input: PullResp) -> anyhow::Result<TxPkt> {
    use std::str::FromStr;

    Ok(TxPkt {
        freq_hz: (input.txpk.freq * 1e6) as u32,
        rf_chain: input.txpk.rfch.try_into()?,
        rf_power: input.txpk.powe.expect("powe"),
        datarate: input.txpk.datr,
        coderate: CodingRate::from_str(input.txpk.codr.as_str())
            .map_err(|_e| anyhow::anyhow!(""))?,
        invert_pol: input.txpk.ipol,
        preamble: input.txpk.prea,
        no_crc: input.txpk.ncrc,
        no_header: input.txpk.nhdr,
        payload: bytes::Bytes::copy_from_slice(&input.txpk.data),
        tx_mode: TxMode::Immediate,
    })
}

async fn print_rx_packet(pkt: &nlighten::types::SCStreamMsg) {
    use chrono::SecondsFormat;

    let rx_time = chrono::offset::Utc::now();
    match pkt {
        nlighten::types::SCStreamMsg::RxPkt(pkt) => {
            let gps_time = match pkt.pkt.gps_time {
                Some(gps_time) => gps_time.as_datetime().to_string(),
                None => String::from("none"),
            };

            let gps_pos = match pkt.pkt.pos {
                Some(gps_pos) => gps_pos.to_string(),
                None => String::from("none"),
            };
            println!(
                "----- {} packet: {:04x} -----",
                rx_time.to_rfc3339_opts(SecondsFormat::Micros, false),
                pkt.id
            );
            println!("  freq: {}", pkt.pkt.freq);
            println!("  datr: {}", pkt.pkt.datarate);
            println!("  snr : {}", pkt.pkt.snr);
            println!("  tmst: {}", pkt.pkt.tmst);
            println!("  rssi_sig (dBm): {}", pkt.pkt.rssi);
            println!("  rssi_chn (raw): {}", pkt.rssic);
            println!("  gps time: {}", gps_time);
            println!("  gps_pos : {}", gps_pos);
            println!(
                "  data: {}",
                BASE64_STANDARD_NO_PAD.encode(&pkt.pkt.payload)
            );
            println!("");
        }

        nlighten::types::SCStreamMsg::RxPktSig(pkt) => {
            println!(
                "----- {} sig packet: {:04x} -----",
                rx_time.to_rfc3339_opts(SecondsFormat::Micros, false),
                pkt.id
            );
            println!("  sig: {}", BASE64_STANDARD_NO_PAD.encode(pkt.sig));
            println!("");
        }

        msg => {
            warn!("unhandled type: {:?}", msg);
        }
    }
}

#[tokio::main]
async fn main() -> Result<()> {
    init_logging();

    let cli = Cli::parse();

    let cfg_file = File::open(cli.config.deref())?;
    let reader = json_comments::StripComments::new(cfg_file);
    let root_config: RootConfig = serde_json::from_reader(reader).with_context(|| {
        format!(
            "error parsing config file: {}",
            cli.config.as_path().display()
        )
    })?;

    info!("Starting pktfwd");

    if let Some(reset_pin) = root_config.board.reset_pin {
        reset(reset_pin);
    }

    let mut sc = match root_config.board.com_type {
        ComType::SPI_PROXY => {
            loragw_hw::secure_concentrator::with_spi(&root_config.board.com_path)?
        }
    };

    sc.run(&root_config.board).await?;

    sc.connect(&root_config.board).await?;
    let mac_addr = sc.get_eui(&root_config.board).await?;

    let gwmp_client = GWMPClient::new(&root_config, mac_addr).await?;

    let mut rx_stream = sc.get_rx_stream();

    let mut sigterm = signal(SignalKind::terminate())?;
    let mut sigint = signal(SignalKind::interrupt())?;

    let mut keep_alive = tokio::time::interval(std::time::Duration::from_secs(
        root_config.gateway_conf.keep_alive.into(),
    ));

    loop {
        tokio::select! {

            Some(pkt) = rx_stream.next() => {
                if cli.verbose {
                    print_rx_packet(&pkt).await;
                }
                gwmp_client.send_push(pkt).await;

            },

            Ok((addr, rx)) = gwmp_client.recv() => {
                match rx {
                    gwmp::types::RxType::PullResp(rx) => {
                        info!("tx pkt from: {} {:?}", addr, rx);

                        match to_tx(rx) {
                            Ok(txpk) => {
                                if let Err(e) = sc.send(&root_config.board, txpk).await {
                                    error!("sending tx: {}", e);
                                }
                            },

                            Err(e) => {
                                error!("sending tx: {}", e);
                            }
                        }


                    }

                    gwmp::types::RxType::PullAck => {}
                }
            },

            Err(e) = gwmp_client.recv() => {
                warn!("received bad gwmp pkt: {}", e);
            },

            _ = keep_alive.tick() => {
                gwmp_client.send_keep_alive().await;
            },

            _ = sigterm.recv() => {
                info!("Shutting down due to SIGTERM");
                break;
            },

            _ = sigint.recv() => {
                info!("Shutting down due to SIGINT");
                break;
            },

        }
    }

    info!("shutting down");

    Ok(())
}
