use base64::prelude::*;
use serde::Deserializer;

pub fn serialize<T, S>(data: T, serializer: S) -> Result<S::Ok, S::Error>
where
    S: serde::Serializer,
    T: AsRef<[u8]>,
{
    let encoded_str = BASE64_STANDARD_NO_PAD.encode(data.as_ref());
    serializer.serialize_str(&encoded_str)
}

pub fn deserialize<'de, D>(deserializer: D) -> Result<Vec<u8>, D::Error>
where
    D: Deserializer<'de>,
{
    use serde::de;
    use serde::de::Deserialize;

    let the_str = String::deserialize(deserializer)?;
    BASE64_STANDARD_NO_PAD
        .decode(the_str)
        .map_err(de::Error::custom)
}
