use std::os::unix::prelude::OsStrExt;

use bytes::{BufMut, Bytes, BytesMut};
use loragw_hw::{RxPktType, RxSig, RxSigPacket, MacAddress};
use tokio::net::UdpSocket;

use crate::{
    {cfg::RootConfig},
};

use serde::{Serialize};

use log::{error, info};

#[derive(Debug, Serialize)]
struct PushPacket {
    pub rxpk: Vec<push::RxPk>,
}

#[derive(Debug, Serialize)]
struct PushPacketSig {
    pub key: u32,
    #[serde(with = "base64")]
    pub sig: Box<[u8]>
}

impl From<RxSig> for PushPacketSig {
    fn from(pkt: RxSig) -> Self {
        Self {
            key: pkt.key,
            sig: pkt.sig,
        }
    }
}

mod push {

    use super::{
        base64,
        MacAddress,
    };
    use serde::Serialize;
    use loragw_hw::{RxSigPacket, RxSigPktData, WGS84Position, Datarate, GPSTime};
    use serde_repr::{Serialize_repr};

    #[derive(Debug, Serialize)]
    pub struct RxPk {
        pub key: u32,
        pub pos: Option<WGS84Position>,
        pub snr: i32,
        pub freq: u32,
        pub rssis: u8,
        pub rssic: u8,
        pub tmst: u32,
        pub datr: Datarate,
        pub gps_time: Option<GPSTime>,
        #[serde(with = "base64")]
        pub data: Box<[u8]>,
        pub stat: CRC,
    }

    impl From<RxSigPacket> for RxPk {
        fn from(pkt: RxSigPacket) -> Self {
            Self { 
                key: pkt.key,
                pos: pkt.pkt.pos,
                snr: pkt.pkt.snr,
                freq: pkt.pkt.freq,
                rssis: pkt.pkt.rssi,
                rssic: pkt.pkt.rssic,
                tmst: pkt.pkt.tmst,
                datr: pkt.pkt.datarate,
                gps_time: pkt.pkt.gps_time,
                data: pkt.pkt.payload,
                stat: if pkt.pkt.crc_en {
                    if pkt.pkt.crc_err {
                        CRC::Fail
                    } else {
                        CRC::OK
                    }
                } else {
                    CRC::Disabled
                }
            }
        }
    }

    #[derive(Debug, Serialize_repr, Clone, PartialEq)]
    #[repr(i8)]
    pub enum CRC {
        Disabled = 0,
        OK = 1,
        Fail = -1,
    }

    
}



mod base64 {

    pub fn serialize<T, S>(data: T, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
        T: AsRef<[u8]>,
    {
        let encoded_str = ::base64::encode(data.as_ref());
        serializer.serialize_str(&encoded_str)
    }

    pub fn deserialize<'de, D>(deserializer: D) -> Result<Vec<u8>, D::Error>
    where
        D: Deserializer<'de>,
    {
        use serde::de;
        use serde::de::Deserialize;

        let the_str = String::deserialize(deserializer)?;
        ::base64::decode(the_str).map_err(de::Error::custom)
    }

    
}

pub struct PktFwdClient {
    socket: UdpSocket,
    gateway_mac: MacAddress,
}

impl PktFwdClient {
    pub async fn new(root_cfg: &RootConfig, gateway_mac: MacAddress) -> anyhow::Result<Self> {
        info!("UDP Client bind: {}", &root_cfg.gateway_conf.local_address);
        let socket = UdpSocket::bind(&root_cfg.gateway_conf.local_address).await?;

        info!("UDP Client connect: {}", &root_cfg.gateway_conf.server_address);
        socket
            .connect(&root_cfg.gateway_conf.server_address)
            .await?;
        Ok(Self {
            socket: socket,
            gateway_mac: gateway_mac,
        })
    }

    pub async fn send_push(&self, pkt: RxPktType) {
        info!("sending {:?}", pkt);
        match pkt {
            RxPktType::RxPkt(pkt) => {
                assert_eq!(self.gateway_mac, pkt.pkt.gatwy_id);
                let token = rand::random();
                match create_push_data(token, &self.gateway_mac, pkt) {
                    Ok(buf) => {
                        if let Err(e) = self.socket.send(&buf).await {
                            error!("error sending udp: {}", e);
                        }
                    }
                    Err(e) => error!("{}", e),
                }
            }
            RxPktType::RxPktSig(sig) => {
                let token = rand::random();
                match create_push_data_sig(token, &self.gateway_mac, sig) {
                    Ok(buf) => {
                        if let Err(e) = self.socket.send(&buf).await {
                            error!("error sending udp: {}", e);
                        }
                    }
                    Err(e) => error!("{}", e),
                }
            }
        }
    }

    pub async fn send_keep_alive(&self) {
        let token = rand::random();
        match create_keep_alive(token, &self.gateway_mac) {
            Ok(buf) => {
                if let Err(e) = self.socket.send(&buf).await {
                    error!("error sending keep alive: {}", e);
                }
            }
            Err(e) => error!("{}", e),
        }
    }
}

fn create_push_data(
    token: u16,
    gateway_id: &MacAddress,
    packet: RxSigPacket,
) -> Result<Bytes, serde_json::Error> {
    let mut buf = BytesMut::with_capacity(1024);
    buf.put_u8(3); // protocol version
    buf.put_u16(token);
    buf.put_u8(0x00); // PUSH_DATA id
    buf.put(gateway_id.as_bytes());

    let data = PushPacket {
        rxpk: vec![ packet.into() ]
    };

    let mut serializer = serde_json::Serializer::new(buf.writer());
    data.serialize(&mut serializer)?;
    Ok(serializer.into_inner().into_inner().freeze())
}

fn create_push_data_sig(
    token: u16,
    gateway_id: &MacAddress,
    packet: RxSig,
) -> Result<Bytes, serde_json::Error> {
    let mut buf = BytesMut::with_capacity(1024);
    buf.put_u8(3); // protocol version
    buf.put_u16(token);
    buf.put_u8(0x06); // PUSH_DATA_SIG id
    buf.put(gateway_id.as_bytes());

    let data = PushPacketSig::from(packet);

    let mut serializer = serde_json::Serializer::new(buf.writer());
    data.serialize(&mut serializer)?;
    Ok(serializer.into_inner().into_inner().freeze())
}

fn create_keep_alive(token: u16, gateway_id: &MacAddress) -> Result<Bytes, serde_json::Error> {
    let mut buf = BytesMut::with_capacity(16);
    buf.put_u8(3); // protocol version
    buf.put_u16(token);
    buf.put_u8(0x02); // PULL_DATA id
    buf.put(gateway_id.as_bytes());
    Ok(buf.freeze())
}
