# NLighten Secure Concentrator Packet Forwarder

The packet forwarder is a program running on the host of the Lora gateway that forwards RF packets received by the Secure Concentrator to a server though IP/UDP. This project is based off of the Semtec LoRa Packet Forwarder but with a modified UDP protocol to support Secure Concentrators.

## Building

Parts of this package use bindgen which requires clang tools to be installed.

```
$ apt install llvm-dev libclang-dev clang gcc-arm-linux-gnueabihf
$ rustup target add armv7-unknown-linux-gnueabihf
```

```
$ cargo build --target=armv7-unknown-linux-gnueabihf
```

## Running

`pktfwd` needs a config file to run. Take a look at example `config.json.kompressor.US915.SPI` and modify for your needs.

```
$ ./pktfwd --config config.json.kompressor.US915.SPI
```