
#include <stdint.h>

#include "fw.h"

#include "arb_fw.var"           /* text_arb_sx1302_13_Nov_3 */
#include "agc_fw_sx1250.var"    /* text_agc_sx1250_05_Juillet_2019_3 */
#include "agc_fw_sx1257.var"    /* text_agc_sx1257_19_Nov_1 */

const uint8_t* get_agc_firmware_sx1250()
{
    return agc_firmware_sx1250;
}

const uint8_t* get_arb_firmware()
{
    return arb_firmware;
}