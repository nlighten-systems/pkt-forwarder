#pragma once

#include <stdint.h>

const uint8_t* get_agc_firmware_sx1250();
const uint8_t* get_arb_firmware();