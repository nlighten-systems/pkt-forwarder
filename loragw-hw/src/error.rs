#[derive(thiserror::Error, Debug)]
pub enum Error {
    #[error("Gateway error: {0}")]
    GatewayError(GatewayError),

    #[error("{0}")]
    Custom(String),

    #[error("{source}")]
    Serialize {
        #[from]
        source: borsh_serde::Error,
    },

    #[error("{source}")]
    Io {
        #[from]
        source: std::io::Error,
    },
}

#[derive(thiserror::Error, Debug)]
pub enum GatewayError {
    #[error("Communication error: {ctx} {source}")]
    Communication { ctx: String, source: std::io::Error },

    #[error("Radio malfunction: {0}")]
    Radio(String),

    #[error("unknown error")]
    Unknown,

    #[error("{0}")]
    Config(String),

    #[error("parse error")]
    ParseError,

    #[error("Timeout")]
    Timeout,
}
impl Error {
    pub(crate) fn custom<T: AsRef<str>>(arg: T) -> Error {
        Error::Custom(String::from(arg.as_ref()))
    }
}

impl<T> From<tokio::sync::mpsc::error::SendError<T>> for Error {
    fn from(_e: tokio::sync::mpsc::error::SendError<T>) -> Self {
        Error::GatewayError(GatewayError::Unknown)
    }
}

impl From<GatewayError> for Error {
    fn from(e: GatewayError) -> Self {
        Error::GatewayError(e)
    }
}
