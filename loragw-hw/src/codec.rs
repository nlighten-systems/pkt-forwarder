use bytes::{Buf, BytesMut};
use serde::Deserialize;

pub fn decode<T>(buf: &mut BytesMut) -> Option<T>
where
    for<'de> T: Deserialize<'de>,
{
    match borsh_serde::de::take_from_bytes(&buf) {
        Ok((v, remaining)) => {
            buf.advance(buf.len() - remaining.len());
            Some(v)
        }

        Err(_e) => None,
    }
}

#[cfg(test)]
mod test {

    use bytes::BufMut;
    use serde::{Deserialize, Serialize};

    use super::*;

    #[derive(Debug, Serialize, Deserialize)]
    pub struct TestMsg {
        id: i32,
        msg: String,
    }

    fn push<T>(v: &T, buf: &mut BytesMut)
    where
        T: Serialize,
    {
        let mut tmp_buf = [0_u8; 128];
        let buf_value = borsh_serde::to_slice(v, &mut tmp_buf).unwrap();
        buf.put_slice(buf_value);
    }

    #[test]
    fn test_decode() {
        let mut buf = bytes::BytesMut::new();
        push(
            &TestMsg {
                id: 4,
                msg: String::from("hello world"),
            },
            &mut buf,
        );

        let decode1: Option<TestMsg> = decode(&mut buf);
        assert!(decode1.is_some());

        push(
            &TestMsg {
                id: 5,
                msg: String::from("hello world 2"),
            },
            &mut buf,
        );

        let decode2: Option<TestMsg> = decode(&mut buf);
        assert!(decode2.is_some());
    }
}
