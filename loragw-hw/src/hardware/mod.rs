pub mod smcu;
mod sx1250;
mod sx130x;

use crate::lib::*;
use async_trait::async_trait;

pub use sx1250::{SX1250OpCode, SX1250};
pub use sx130x::{SX130x, SX130xConfig};

#[async_trait]
pub trait Interface {
    async fn sx130x_read_register(&self, addr: u16) -> Result<u8, Error>;
    async fn sx130x_write_register(&self, addr: u16, value: u8) -> Result<(), Error>;
    async fn sx130x_rmw_register(&self, addr: u16, value: u8, mask: u8) -> Result<(), Error>;
    async fn sx130x_mem_write(&self, addr: u16, data: bytes::Bytes) -> Result<(), Error>;
    async fn sx130x_mem_read(
        &self,
        addr: u16,
        len: u16,
        fifo_mode: bool,
    ) -> Result<bytes::Bytes, Error>;
    async fn sx1250_read(
        &self,
        radio: Radio,
        op_code: SX1250OpCode,
        out_data: &mut [u8],
    ) -> Result<(), Error>;
    async fn sx1250_write(
        &self,
        radio: Radio,
        op_code: SX1250OpCode,
        out_data: &[u8],
    ) -> Result<(), Error>;
    async fn smcu_read(&self, addr: u16) -> Result<u32, Error>;
    async fn smcu_write(&self, addr: u16, value: u32) -> Result<(), Error>;
}

// pub async fn sx130x_mem_write(interface: &dyn Interface, mut addr: u16, mut data: &[u8]) -> Result<(), Error> {

//     while data.len() > 0 {
//         let chunk = &data[..data.len().min(CHUNK_SIZE_MAX)];
//         interface.sx130x_write_chunk(addr, chunk).await?;
//         addr += chunk.len() as u16;
//         data = &data[chunk.len()..];
//     }

//     Ok(())
// }

// pub async fn sx130x_mem_read(interface: &dyn Interface, mut addr: u16, mut data: &mut [u8], fifo_mode: bool) -> Result<(), Error> {

//     while data.len() > 0 {
//         let chunk_size = data.len().min(CHUNK_SIZE_MAX);
//         {
//             let chunk = &mut data[..chunk_size];
//             interface.sx130x_read_chunk(addr, chunk).await?;

//             if !fifo_mode {
//                 addr += chunk.len() as u16;
//             }
//         }
//         data = &mut data[chunk_size..];
//     }

//     Ok(())
// }
