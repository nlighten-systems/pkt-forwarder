//! # Rust interface library for NLighten Secure Concentrator
//! 
//! This crate provides a high-level interface for LoRaWAN Secure Concentrator.
//! 
//! Before you can begin working with a Secure Concentrator, you need to load its configuration file. See `config.json.kompressor.US915.SPI` for an example config file. Parse and load the config file:
//! ```
//! # use std::fs::File;
//! # use std::io;
//! use loragw_hw::lib::*;
//! 
//! # async fn foo() -> anyhow::Result<()> {
//! let cfg_file = File::open("config.json.kompressor.US915.SPI")?;
//! let root_config: cfg::RootConfig = serde_json::from_reader(cfg_file)?;
//! let mut sc = secure_concentrator::with_spi(&root_config.board.com_path)?;
//!
//! // Start the message handler 
//! sc.run(&root_config.board).await?;
//! // Connect to the Concentrator card and load its configuration
//! sc.connect(&root_config.board).await?;
//! # Ok(())
//! # }
//! ```
//! 
//! ## Send LoRa Packet
//! ```
//! # use std::fs::File;
//! # use std::io;
//! # use std::str::FromStr;
//! # use bytes::Bytes;
//! # use loragw_hw::lib::*;
//! 
//! # async fn foo() -> anyhow::Result<()> {
//! # let cfg_file = File::open("config.json.kompressor.US915.SPI")?;
//! # let root_config: cfg::RootConfig = serde_json::from_reader(cfg_file)?;
//! # let mut sc = secure_concentrator::with_spi(&root_config.board.com_path)?;
//! let pkt = TxPkt {
//!     freq_hz: 920_200_000,
//!     rf_chain: 0, // should always be 0
//!     rf_power: 15, // Tranmit power (dBm)
//!     datarate: Datarate::from_str("SF7BW125")?,
//!     coderate: CodingRate::CR_4_5,
//!     invert_pol: false,
//!     preamble: None,
//!     no_crc: false,
//!     no_header: false,
//!     payload: Bytes::from(&b"hello world"[..]),
//!     tx_mode: TxMode::Immediate,
//! };
//! 
//! sc.send(&root_config.board, pkt).await;
//! # Ok(())
//! # }
//! ```
//! 
//! ## Receive LoRa packets
//! ```
//! # use std::fs::File;
//! # use std::io;
//! # use futures::{prelude::*, FutureExt};
//! # use loragw_hw::lib::nlighten::types::SCStreamMsg;
//! # use loragw_hw::lib::*;
//! # async fn foo() -> anyhow::Result<()> {
//! # let cfg_file = File::open("config.json.kompressor.US915.SPI")?;
//! # let root_config: cfg::RootConfig = serde_json::from_reader(cfg_file)?;
//! # let mut sc = secure_concentrator::with_spi(&root_config.board.com_path)?;
//! let mut rx_stream = sc.get_rx_stream();
//! 
//! loop {
//!   match rx_stream.next().await {
//!     Some(SCStreamMsg::RxPkt(ptk)) => {
//!       // handle received LoRa packet
//!     }
//! 
//!     Some(SCStreamMsg::RxPktSig(pkt)) => {
//!       // handle signature of previously received LoRa packet
//!     }
//! 
//!     // ...
//! 
//!     # _ => {  }
//!   }
//! }
//! # Ok(())
//! # }
//! ```

mod binding;
pub mod cfg;
mod codec;
mod error;
mod hardware;
pub mod secure_concentrator;
mod types;

#[cfg(feature = "spi")]
mod spi;

pub mod lib {
    pub use super::error::*;
    pub use crate::cfg;
    pub use crate::hardware::*;
    pub use crate::secure_concentrator;
    pub use crate::types::*;
    pub use log::{debug, error, info, warn};
    pub use nlighten::{
        self,
        gps::{GPSTime, WGS84Position},
        lora::{Bandwidth, CodingRate, Datarate, MacAddress, SpreadingFactor},
    };
}

pub use types::BoardConfig;
