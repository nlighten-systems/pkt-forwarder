use crate::lib::*;
use int_enum::IntEnum;
use nlighten::{
    gps::{GPSTime, WGS84Position},
    prelude::Signature,
};
use serde::{Deserialize, Serialize};
use std::{array::TryFromSliceError, collections::HashMap};

#[derive(Debug, Clone)]
pub struct SX1302Version {
    pub major: u8,
    pub minor: u8,
}

impl std::fmt::Display for SX1302Version {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "v{}.{}", self.major, self.minor)
    }
}

#[derive(Debug, Deserialize, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub enum FineTimestampMode {
    HighCapacity, // fine timestamps for SF5 -> SF10
    AllSF,        // fine timestampsl for SF5 -> SF12
}

#[repr(u8)]
#[derive(IntEnum, Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub enum SX130xModel {
    SX1302 = 0x02,
    SX1303 = 0x03,
}

#[derive(Debug, Deserialize, PartialEq, Eq, Clone, Copy)]
pub enum RadioType {
    None,
    SX1255,
    SX1257,
    SX1272,
    SX1276,
    SX1250,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub enum Radio {
    RadioA = 1,
    RadioB = 2,
}

#[derive(Debug, Deserialize, PartialEq, Eq, Clone, Copy)]
pub struct TXGain {
    /// measured TX power at the board connector, in dBm
    pub rf_power: i8,

    /// (sx125x) 2 bits: control of the external PA (SX1302 I/O)
    /// (sx1250) 1 bits: enable/disable the external PA (SX1302 I/O)
    pub pa_gain: u8,

    /// (sx1250) 6 bits: control the radio power index to be used for configuration
    pub pwr_idx: u8,

    /// (sx125x) 2 bits: control of the digital gain of SX1302
    #[serde(default)]
    pub dig_gain: u8,
}

#[repr(u8)]
#[derive(IntEnum, Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub enum Modulation {
    CW = 0x08,
    Lora = 0x10,
    FSK = 0x20,
}

#[derive(Debug, Deserialize, PartialEq, Clone)]
pub struct RFChainConfig {
    pub enable: bool,

    #[serde(rename = "freq")]
    pub freq_hz: u32, // center frequency of the radio in Hz
    pub rssi_offset: f32,

    #[serde(rename = "type")]
    pub radio_type: RadioType,
    pub tx_enable: bool, // enable or disable TX on that RF chain

    #[serde(default)]
    pub single_input_mode: bool, // Configure the radio in single or differential input mode (SX1250 only)

    pub tx_gain_lut: Option<Vec<TXGain>>,
}

#[derive(Debug, Default, Deserialize, PartialEq, Clone, Copy)]
pub struct IFChainConfig {
    pub enable: bool,

    #[serde(rename = "radio")]
    pub rf_chain: u8,

    #[serde(rename = "if")]
    pub relative_freq: i32,

    pub bandwidth: Option<u32>,

    pub spread_factor: Option<SpreadingFactor>,

    pub sync_word_size: Option<u8>,

    pub implicit_hdr: Option<bool>,

    pub implicit_payload_length: Option<u8>,

    pub implicit_crc_en: Option<bool>,

    pub implicit_coderate: Option<u8>,
}

#[derive(Debug, Deserialize, PartialEq)]
pub struct BoardConfig {
    pub reset_pin: Option<u32>,
    pub com_type: ComType,
    pub com_path: String,
    pub clksrc: u8, // index of the RF chain which provides clock to the concentrator
    pub radio_0: RFChainConfig,
    pub radio_1: RFChainConfig,

    #[serde(flatten)]
    pub extra: HashMap<String, serde_json::Value>,
}

#[derive(Debug, Serialize, Deserialize, PartialEq, Eq, PartialOrd, Ord)]
pub enum ComType {
    SPI_PROXY,
}

#[derive(Debug, Serialize, Deserialize)]
pub enum RxPktType {
    RxPkt(RxSigPacket),
    RxPktSig(RxSig),
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct RxSigPacket {
    pub key: u32,
    pub pkt: RxSigPktData,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct RxSigPktData {
    pub freq: u32,
    pub datarate: Datarate,
    pub snr: i32,
    pub rssi: u8,
    pub rssic: u8,
    pub tmst: u32,
    pub gatwy_id: MacAddress,
    pub crc_en: bool,
    pub crc_err: bool,
    pub gps_time: Option<GPSTime>,
    pub pos: Option<WGS84Position>,
    pub payload: Box<[u8]>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct RxSig {
    pub key: u32,
    pub sig: Box<[u8]>,
}

impl RxSig {
    pub fn signature(&self) -> Result<Signature, TryFromSliceError> {
        Ok(Signature::new(self.sig.as_ref().try_into()?))
    }
}

pub struct TxPkt {
    /// center frequency of TX
    pub freq_hz: u32,
    pub rf_chain: u8,
    /// TX power, in dBm
    pub rf_power: i8,

    pub datarate: Datarate,
    pub coderate: CodingRate,
    /// invert signal polarity, for orthogonal downlinks (LoRa only)
    pub invert_pol: bool,
    pub preamble: Option<u16>,
    pub no_crc: bool,
    pub no_header: bool,
    pub payload: bytes::Bytes,
    pub tx_mode: TxMode,
}

#[derive(Debug)]
pub enum TxMode {
    Immediate,
    Timestamped(u32),
    OnGPS,
}
