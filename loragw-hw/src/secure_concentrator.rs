use crate::{codec, lib::*};
use nlighten::{
    prelude::{PublicKey}, protocol::*, serialnum::SerialNum, types::SCStreamMsg,
    version_number::VersionNumber,
};

use async_trait::async_trait;
use bytes::{BufMut, BytesMut};
use futures::Stream;
use std::{io::Cursor, path::Path};
use take_if::TakeIf;
use tokio::sync::oneshot;
use tokio_stream::wrappers::ReceiverStream;

#[allow(dead_code)]
const FW_VERSION_AGC_SX1250: i32 = 10; /* Expected version of AGC firmware for sx1250 based gateway */
/* v10 is same as v6 with improved channel check time for LBT */
#[allow(dead_code)]
const FW_VERSION_AGC_SX125X: i32 = 6; /* Expected version of AGC firmware for sx1255/sx1257 based gateway */
#[allow(dead_code)]
const FW_VERSION_ARB: i32 = 2; /* Expected version of arbiter firmware */

pub type MessageReceiver = tokio::sync::mpsc::Receiver<Msg>;
pub type MessageSender = tokio::sync::mpsc::Sender<Msg>;

pub struct MessageStream {
    pub tx: tokio::sync::mpsc::Sender<Msg>,
    pub rx: tokio::sync::mpsc::Receiver<Msg>,
}

#[cfg(feature = "spi")]
pub fn with_spi<S: AsRef<Path>>(path: S) -> Result<SecureConcentrator, Error> {
    Ok(SecureConcentrator::new(crate::spi::SpiStream::new(path)?))
}

pub struct SecureConcentrator {
    state: State,
    cmds: tokio::sync::mpsc::Sender<Command>,
    rx_msg: Option<tokio::sync::mpsc::Receiver<SCStreamMsg>>,
    sc_serial_num: Option<SerialNum>,
    sc_firmware_version: Option<VersionNumber>,
    sc_pubkey: Option<PublicKey>,
    sign_mutex: tokio::sync::Mutex<()>,
}

enum State {
    Init(Inner),
    Running,
}

struct Inner {
    lower: MessageStream,
    pending_sx130x_read: Option<SX130XReadCmd>,
    pending_sx1250_read: Option<SX1250ReadCmd>,
    pending_sx130x_memread: Option<SX130xMemReadCmd>,
    pending_smcu_read: Option<SMCUReadCmd>,
    cmds: tokio::sync::mpsc::Receiver<Command>,
    rx_msg: tokio::sync::mpsc::Sender<SCStreamMsg>,
    stream_0_buf: bytes::BytesMut,
}

impl Inner {
    async fn handle_cmd(&mut self, cmd: Command) -> Result<(), Error> {
        debug!("handle_cmd: {:?}", cmd);
        match cmd {
            Command::SX130xRegRead(r) => {
                if self.pending_sx130x_read.is_some() {
                    return Err(Error::custom(format!(
                        "can only handle one read operation at a time"
                    )));
                }
                let address = r.addr;
                self.pending_sx130x_read = Some(r);

                self.lower.tx.send(Msg::SX130xRegRead(address)).await
            }

            Command::SMCURead(r) => {
                if self.pending_smcu_read.is_some() {
                    return Err(Error::custom(format!(
                        "can only handle one read operation at a time"
                    )));
                }
                let address = r.addr;
                self.pending_smcu_read = Some(r);
                self.lower.tx.send(Msg::SMCURegRead(address)).await
            }

            Command::SX130xRegWrite(w) => self.lower.tx.send(Msg::SX130xRegWrite(w)).await,

            Command::SX130xRegRMW(rmw) => self.lower.tx.send(Msg::SX130xRegRMW(rmw)).await,

            Command::SX130xMemWrite((addr, data)) => {
                self.lower
                    .tx
                    .send(Msg::SX130xMemWrite(SX130xMemWrite {
                        address: addr,
                        len: data
                            .len()
                            .try_into()
                            .map_err(|_e| Error::custom(format!("too big")))?,
                    }))
                    .await?;

                //let mut addr = addr;
                let mut data = &data[..];
                while data.len() > 0 {
                    let chunk = &data[..data.len().min(MsgChunk::CHUNK_SIZE_MAX)];
                    let chunk = MsgChunk::try_from(chunk).unwrap();
                    self.lower.tx.send(Msg::Data(chunk)).await?;
                    //addr += chunk.len() as u16;
                    data = &data[chunk.len()..];
                }

                Ok(())
            }

            Command::SX130xMemRead(r) => {
                if self.pending_sx130x_memread.is_some() {
                    return Err(Error::custom(format!(
                        "can only handle one read operation at a time"
                    )));
                }

                let address = r.addr;
                let len = r.len;
                let fifo_mode = r.fifo_mode;
                self.pending_sx130x_memread = Some(r);
                self.lower
                    .tx
                    .send(Msg::SX130xMemRead(SX130xMemRead {
                        address,
                        len,
                        fifo_mode,
                    }))
                    .await
            }

            Command::SX1250Read(r) => {
                if self.pending_sx1250_read.is_some() {
                    return Err(Error::custom(format!(
                        "can only handle one read operation at a time"
                    )));
                }
                let read_op = r.read_op;
                self.pending_sx1250_read = Some(r);
                self.lower.tx.send(Msg::SX1250Read(read_op)).await
            }

            Command::SX1250Write(w) => self.lower.tx.send(Msg::SX1250Write(w)).await,

            Command::SMCUWrite(w) => self.lower.tx.send(Msg::SMCUWrite(w)).await,
        }?;
        Ok(())
    }

    async fn handle_msg(&mut self, msg: Msg) -> Result<(), Error> {
        debug!("handle: {:?}", msg);
        match msg {
            Msg::SX130xRegValue(v) => {
                if let Some(r) = self.pending_sx130x_read.take_if(|x| x.addr == v.address) {
                    if let Err(_e) = r.tx.send(v.value) {
                        return Err(Error::custom(format!(
                            "unable to send pending sx130x read: {:?}",
                            r.addr
                        )));
                    }
                }
            }

            Msg::SX1250Data(r, buf) => {
                if let Some(r) = self
                    .pending_sx1250_read
                    .take_if(|x| x.read_op.radio == r.radio)
                {
                    if let Err(_e) = r.tx.send(buf) {
                        return Err(Error::custom(format!("unable to send pending sx1250 read")));
                    }
                }
            }

            Msg::SMCURegValue(v) => {
                if let Some(r) = self.pending_smcu_read.take_if(|x| x.addr == v.address) {
                    if let Err(_) = r.tx.send(v.value) {
                        return Err(Error::custom(format!(
                            "unable to send pending smcu read: {}",
                            r.addr
                        )));
                    }
                }
            }

            Msg::Data(d) => {
                if let Some(r) = self.pending_sx130x_memread.as_mut() {
                    r.buf.put_slice(&d);
                }

                if let Some(r) = self
                    .pending_sx130x_memread
                    .take_if(|x| x.buf.len() >= x.len as usize)
                {
                    if let Err(_e) = r.tx.send(r.buf.freeze()) {
                        return Err(Error::custom(format!(
                            "unable to send pending SX130xMemData"
                        )));
                    }
                }
            }

            Msg::StreamData(d, chunk) => match d {
                0 => {
                    self.stream_0_buf.put_slice(&chunk);
                    match codec::decode::<SCStreamMsg>(&mut self.stream_0_buf) {
                        Some(v) => self.rx_msg.send(v).await?,

                        None => {}
                    }
                }

                o => warn!("unhandled data from stream: {}", o),
            },

            m => {
                warn!("unhandled msg: {:?}", m);
            }
        }
        Ok(())
    }
}

#[derive(Debug)]
enum Command {
    SX130xRegRead(SX130XReadCmd),
    SX130xRegWrite(SX130xRegValue),
    SX130xRegRMW(SX130xRegRMW),
    SX130xMemWrite((u16, bytes::Bytes)),
    SX130xMemRead(SX130xMemReadCmd),
    SX1250Read(SX1250ReadCmd),
    SX1250Write(SX1250Write),
    SMCURead(SMCUReadCmd),
    SMCUWrite(SMCURegValue),
}

struct AsyncInter<'a> {
    inner: &'a SecureConcentrator,
}

#[async_trait]
impl<'a> Interface for AsyncInter<'a> {
    async fn sx130x_read_register(&self, addr: u16) -> Result<u8, Error> {
        self.inner.sx130x_read_reg(addr).await
    }

    async fn sx130x_write_register(&self, addr: u16, value: u8) -> Result<(), Error> {
        self.inner.sx130x_write_reg(addr, value).await
    }

    async fn sx130x_rmw_register(&self, addr: u16, value: u8, mask: u8) -> Result<(), Error> {
        self.inner.sx130x_rmw_reg(addr, value, mask).await
    }

    async fn sx130x_mem_write(&self, addr: u16, data: bytes::Bytes) -> Result<(), Error> {
        self.inner.sx130x_mem_write(addr, data).await
    }

    async fn sx130x_mem_read(
        &self,
        addr: u16,
        len: u16,
        fifo_mode: bool,
    ) -> Result<bytes::Bytes, Error> {
        self.inner.sx130x_mem_read(addr, len, fifo_mode).await
    }

    async fn sx1250_read(
        &self,
        radio: Radio,
        op_code: SX1250OpCode,
        out_data: &mut [u8],
    ) -> Result<(), Error> {
        self.inner.sx1250_read(radio, op_code, out_data).await
    }

    async fn sx1250_write(
        &self,
        radio: Radio,
        op_code: SX1250OpCode,
        out_data: &[u8],
    ) -> Result<(), Error> {
        self.inner.sx1250_write(radio, op_code, out_data).await
    }

    async fn smcu_read(&self, addr: u16) -> Result<u32, Error> {
        self.inner.smcu_read_reg(addr).await
    }

    async fn smcu_write(&self, addr: u16, value: u32) -> Result<(), Error> {
        self.inner.smcu_write_reg(addr, value).await
    }
}

impl SecureConcentrator {
    pub fn new(stream: MessageStream) -> Self {
        let (tx, rx) = tokio::sync::mpsc::channel(5);
        let (tx_msg, rx_msg) = tokio::sync::mpsc::channel(5);
        SecureConcentrator {
            state: State::Init(Inner {
                lower: stream,
                pending_sx130x_read: None,
                pending_sx1250_read: None,
                pending_sx130x_memread: None,
                pending_smcu_read: None,
                cmds: rx,
                rx_msg: tx_msg,
                stream_0_buf: bytes::BytesMut::new(),
            }),
            cmds: tx,
            rx_msg: Some(rx_msg),
            sc_serial_num: None,
            sc_firmware_version: None,
            sc_pubkey: None,
            sign_mutex: tokio::sync::Mutex::new(()),
        }
    }

    pub async fn run(&mut self, _board_cfg: &BoardConfig) -> Result<(), Error> {
        if let State::Init(mut inner) = std::mem::replace(&mut self.state, State::Running) {
            tokio::spawn(async move {
                loop {
                    tokio::select! {
                        Some(cmd) = inner.cmds.recv() => {
                            if let Err(e) = inner.handle_cmd(cmd).await {
                                error!("{}", e);
                                return;
                            }
                        },

                        Some(msg) = inner.lower.rx.recv() => {
                            if let Err(e) = inner.handle_msg(msg).await {
                                error!("{}", e);
                                return;
                            }
                        }

                    }
                }
            });

            Ok(())
        } else {
            Err(Error::custom("can start only once"))
        }
    }

    pub fn get_rx_stream(&mut self) -> impl Stream<Item = SCStreamMsg> {
        ReceiverStream::new(self.rx_msg.take().unwrap())
    }

    pub async fn connect(&mut self, board_cfg: &BoardConfig) -> Result<(), Error> {
        let interface = AsyncInter { inner: self };

        let sx130x_cfg = SX130xConfig::from_board_cfg(board_cfg)?;

        let sx130x = SX130x::new(&interface, &sx130x_cfg);

        let sc_serial_num = SerialNum::from(interface.smcu_read(smcu::REG_HARDWARE_SERIAL).await?);
        info!("Secure Concentrator serial: {}", sc_serial_num);

        let sc_firmware_version = VersionNumber(interface.smcu_read(smcu::REG_FIRMWARE_VERSION).await?);
        info!("firmware: {}", sc_firmware_version);

        let mut cursor = Cursor::new([0_u8; 32]);
        for i in 0..8 {
            use std::io::Write;

            let v = interface.smcu_read(0x10 + i).await?;
            let data = v.to_be_bytes();
            cursor.write_all(&data)?;
        }

        let sc_pub_key = PublicKey::new(cursor.into_inner());
        info!("sc_pubkey: {:02x?}", sc_pub_key);

        let v = sx130x.hw_version().await?;
        info!("SX130x: {}", v);

        /* Set all GPIOs to 0 */
        sx130x.set_gpio(0x00).await?;

        /* Calibrate radios */
        sx130x.radio_calibrate().await?;

        /* Setup radios for RX */
        for (i, chain) in sx130x_cfg
            .rf_chain_cfg
            .iter()
            .filter(|c| c.enable)
            .enumerate()
        {
            let radio = match i {
                0 => Radio::RadioA,
                1 => Radio::RadioB,
                o => panic!("unknown radio index: {}", o),
            };

            sx130x.radio_reset(i).await?;

            /* Setup the radio */
            match chain.radio_type {
                RadioType::SX1250 => {
                    SX1250::setup(&interface, radio, chain.freq_hz, chain.single_input_mode)
                        .await?;
                }
                o => {
                    todo!("support for {:?}", o);
                }
            }

            /* Set radio mode */
            sx130x.radio_set_mode(i).await?;
        }

        /* Select the radio which provides the clock to the sx1302 */
        sx130x.radio_clock_select().await?;

        /* Release host control on radio (will be controlled by AGC) */
        sx130x.radio_host_ctrl(false).await?;

        /* Basic initialization of the sx1302 */
        sx130x.init().await?;

        /* Configure PA/LNA LUTs */
        sx130x.pa_lna_lut_config().await?;

        /* Configure Radio FE */
        sx130x.radio_fe_configure().await?;

        /* Configure the Channelizer */
        sx130x.channelizer_configure(false).await?;

        /* configure LoRa 'multi-sf' modems */
        sx130x.lora_correlator_configure().await?;
        sx130x.lora_modem_configure().await?;

        /* configure LoRa 'single-sf' modem */
        if sx130x.config.if_chain_cfg[8].enable {
            sx130x.lora_service_correlator_configure().await?;
            sx130x.lora_service_modem_configure().await?;
        }

        /* TODO: configure FSK modem */
        if sx130x.config.if_chain_cfg[9].enable {
            todo!("Support FSK modem");
            // err = sx1302_fsk_configure(&(CONTEXT_FSK));
            // if (err != LGW_REG_SUCCESS) {
            //     printf("ERROR: failed to configure SX1302 FSK modem\n");
            //     return LGW_HAL_ERROR;
            // }
        }

        /* configure syncword */
        sx130x.lora_syncword().await?;

        /* enable demodulators - to be done before starting AGC/ARB */
        sx130x.modem_enable().await?;

        /* Load AGC firmware */
        let fw_version_agc;
        match sx130x.config.rf_chain_cfg[sx130x.config.clksrc as usize].radio_type {
            RadioType::SX1250 => {
                info!("Loading AGC firmware for SX1250");
                sx130x.agc_load_firmware(SX1250::agc_firmware()).await?;
                fw_version_agc = FW_VERSION_AGC_SX1250;
            }
            RadioType::SX1255 | RadioType::SX1257 => {
                todo!("support SX1255 and SX1257")
            }
            o => {
                return Err(Error::from(GatewayError::Config(format!(
                    "failed to load AGC firmware. Radio type not supported: {:?}",
                    o
                ))))
            }
        }

        sx130x.agc_start(fw_version_agc).await?;

        /* Load ARB firmware */
        sx130x.arb_load_firmware(SX130x::arb_firmware()).await?;
        sx130x.arb_start().await?;

        /* static TX configuration */
        sx130x.tx_configure().await?;

        sx130x.gps_enable(true).await?;

        sx130x.set_gpio(0x01).await?;

        //let gateway_model = SX1302::get_model(&mut self.com)?;

        let eui = sx130x.get_eui().await?;
        info!("gateway eui: {}", eui);

        // set the AFRE bit (Automatic FIFO Read Enable)
        interface
            .smcu_write(smcu::REG_CTRL, smcu::CONTROL_AFRE)
            .await?;

        //self.read_sx130x_fifo(sx130x).await;
        //loop {
        //    tokio::time::sleep(std::time::Duration::from_secs(1)).await;
        //}

        self.sc_serial_num = Some(sc_serial_num);
        self.sc_firmware_version = Some(sc_firmware_version);
        self.sc_pubkey = Some(sc_pub_key);

        Ok(())
    }

    /// The SX130x globally unique identifier
    pub async fn get_eui(&self, board_cfg: &BoardConfig) -> anyhow::Result<MacAddress> {
        let interface = AsyncInter { inner: self };
        let sx130x_cfg = SX130xConfig::from_board_cfg(board_cfg)?;
        let sx130x = SX130x::new(&interface, &sx130x_cfg);

        let eui = sx130x.get_eui().await?;
        info!("gateway eui: {}", eui);
        Ok(eui)
    }

    /// The Secure Concentrator's Serial Number
    pub fn sc_serial_num(&self) -> Option<SerialNum> {
        self.sc_serial_num
    }

    /// The Secure Concentrator's SMCU Firmware version
    pub fn sc_firmware_version(&self) -> Option<VersionNumber> {
        self.sc_firmware_version
    }

    /// The Secure Concentrator's public ED25519 key
    pub fn sc_pubkey(&self) -> Option<PublicKey> {
        self.sc_pubkey
    }

    /// Transmit a LoRa packet
    pub async fn send(&self, board_cfg: &BoardConfig, pkt: TxPkt) -> Result<(), Error> {
        let interface = AsyncInter { inner: self };
        let sx130x_cfg = SX130xConfig::from_board_cfg(board_cfg)?;
        let sx130x = SX130x::new(&interface, &sx130x_cfg);
        sx130x.send(pkt).await?;
        Ok(())
    }

    pub async fn siqn_non_rf(&self, data: &[u8]) -> Result<Vec<u8>, Error> {
        let interface = AsyncInter { inner: self };

        let _lock = interface.inner.sign_mutex.lock().await;

        let len = data.len() as u32;

        //first set the size of the data
        interface.smcu_write(smcu::REG_NON_RF_SIGN_SIZE, len).await?;

        for chunk in data.chunks(4) {
            let l = chunk.len();
            let mut bytes = [0_u8 ; 4];
            bytes[..l].copy_from_slice(chunk);
            interface.smcu_write(smcu::REG_NON_RF_FIFO, u32::from_be_bytes(bytes)).await?;
        }

        let mut timeout = 10_usize;

        while (interface.smcu_read(smcu::REG_STATUS).await? & smcu::STATUS_NONRF_R) == 0 {
            tokio::time::sleep(std::time::Duration::from_millis(20)).await;
            timeout -= 1;
            if timeout == 0 {
                return Err(Error::custom("signature timeout"));
            }
        }

        let mut cursor = Cursor::new([0_u8; 32]);
        for i in 0..16 {
            use std::io::Write;

            let v = interface.smcu_read(0x20 + i).await?;
            let data = v.to_be_bytes();
            cursor.write_all(&data)?;
        }

        Ok(cursor.into_inner().into())
    }

    pub async fn read_sx130x_fifo(&self, sx130x: SX130x<'_>) {
        loop {
            match sx130x.read_fifo().await {
                Ok(Some(buf)) => {
                    debug!("got fifo bytes {:02X?}", &buf);
                }

                Err(e) => {
                    error!("{}", e);
                }

                _ => {}
            }
            tokio::time::sleep(std::time::Duration::from_millis(10)).await;
        }
    }

    pub async fn sx130x_write_reg(&self, address: u16, value: u8) -> Result<(), Error> {
        debug!("SX130xRegWrite 0x{:04x} 0x{:02x}", address, value);
        self.cmds
            .send(Command::SX130xRegWrite(SX130xRegValue { address, value }))
            .await
            .map_err(|e| Error::custom(format!("{}", e)))
    }

    pub async fn sx130x_read_reg(&self, addr: u16) -> Result<u8, Error> {
        let (tx, rx) = oneshot::channel();

        self.cmds
            .send(Command::SX130xRegRead(SX130XReadCmd { addr, tx }))
            .await
            .map_err(|e| Error::custom(format!("{}", e)))?;

        let v = rx.await.map_err(|e| Error::custom(format!("e: {}", e)))?;

        debug!("SX130xRegRead 0x{:04x} 0x{:02x}", addr, v);

        Ok(v)
    }

    pub async fn sx130x_rmw_reg(&self, address: u16, value: u8, mask: u8) -> Result<(), Error> {
        self.cmds
            .send(Command::SX130xRegRMW(SX130xRegRMW {
                address,
                value,
                mask,
            }))
            .await
            .map_err(|_e| Error::custom(""))
    }

    pub async fn sx130x_mem_write(&self, addr: u16, data: bytes::Bytes) -> Result<(), Error> {
        self.cmds
            .send(Command::SX130xMemWrite((addr, data)))
            .await
            .map_err(|_e| Error::custom(""))
    }

    pub async fn sx130x_mem_read(
        &self,
        addr: u16,
        len: u16,
        fifo_mode: bool,
    ) -> Result<bytes::Bytes, Error> {
        let (tx, rx) = oneshot::channel();

        self.cmds
            .send(Command::SX130xMemRead(SX130xMemReadCmd {
                addr,
                len,
                tx,
                fifo_mode,
                buf: BytesMut::new(),
            }))
            .await
            .map_err(|e| Error::custom(format!("{}", e)))?;

        let v = rx.await.map_err(|e| Error::custom(format!("{}", e)))?;

        Ok(v)
    }

    pub async fn sx1250_read(
        &self,
        radio: Radio,
        op_code: SX1250OpCode,
        out_data: &mut [u8],
    ) -> Result<(), Error> {
        let (tx, rx) = oneshot::channel();

        let radio: u8 = radio as u8;
        let opcode: u8 = op_code as u8;
        let buf_size: u8 = out_data.len() as u8;

        self.cmds
            .send(Command::SX1250Read(SX1250ReadCmd {
                read_op: SX1250Read {
                    radio,
                    opcode,
                    buf_size,
                },
                tx,
            }))
            .await
            .map_err(|e| Error::custom(format!("{}", e)))?;

        let result = rx.await.map_err(|e| Error::custom(format!("{}", e)))?;

        debug!(
            "SX1250Read {} {} {:?}",
            radio,
            opcode,
            &result[..buf_size as usize]
        );

        for i in 0..out_data.len() {
            out_data[i] = result[i];
        }

        Ok(())
    }

    pub async fn sx1250_write(
        &self,
        radio: Radio,
        op_code: SX1250OpCode,
        data: &[u8],
    ) -> Result<(), Error> {
        let radio: u8 = radio as u8;
        let opcode: u8 = op_code as u8;
        let buf_size: u8 = data.len() as u8;
        let mut buf = [0_u8; 5];

        for i in 0..data.len() {
            buf[i] = data[i];
        }

        debug!("SX1250Write {} {} {:?}", radio, opcode, data);

        self.cmds
            .send(Command::SX1250Write(SX1250Write {
                radio,
                opcode,
                buf_size,
                buf,
            }))
            .await
            .map_err(|e| Error::Custom(format!("{}", e)))
    }

    pub async fn smcu_read_reg(&self, addr: u16) -> Result<u32, Error> {
        debug!("SMCUReadReg {:04x}", addr);
        let (tx, rx) = oneshot::channel();

        self.cmds
            .send(Command::SMCURead(SMCUReadCmd { addr, tx }))
            .await
            .map_err(|e| Error::custom(format!("{}", e)))?;

        let v = rx.await.map_err(|e| Error::custom(format!("{}", e)))?;
        Ok(v)
    }

    pub async fn smcu_write_reg(&self, address: u16, value: u32) -> Result<(), Error> {
        debug!("SMCUWriteReg 0x{:04x} 0x{:08x}", address, value);
        self.cmds
            .send(Command::SMCUWrite(SMCURegValue { address, value }))
            .await
            .map_err(|e| Error::custom(format!("{}", e)))
    }
}

#[derive(Debug)]
struct SX130XReadCmd {
    addr: u16,
    tx: oneshot::Sender<u8>,
}

#[derive(Debug)]
struct SMCUReadCmd {
    addr: SMCUAddress,
    tx: oneshot::Sender<u32>,
}

#[derive(Debug)]
struct SX1250ReadCmd {
    read_op: SX1250Read,
    tx: oneshot::Sender<[u8; 5]>,
}

#[derive(Debug)]
struct SX130xMemReadCmd {
    addr: u16,
    len: u16,
    fifo_mode: bool,
    buf: BytesMut,
    tx: oneshot::Sender<bytes::Bytes>,
}
