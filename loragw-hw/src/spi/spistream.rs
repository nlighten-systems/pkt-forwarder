use crate::lib::*;
use nlighten::protocol::{rtp, SpiFrame};
use spidev::{SpiModeFlags, Spidev, SpidevOptions, SpidevTransfer};

use std::{path::Path, time::Duration};

pub struct SpiStream {}

impl SpiStream {
    pub fn new<T: AsRef<Path>>(path: T) -> Result<secure_concentrator::MessageStream, Error> {
        let options = SpidevOptions::new()
            .bits_per_word(8)
            .max_speed_hz(1_000_000)
            .mode(SpiModeFlags::SPI_MODE_0)
            .lsb_first(false)
            .build();

        let mut spi = Spidev::open(path)?;
        spi.configure(&options)?;

        let (tx_sender, mut tx_receiver) = tokio::sync::mpsc::channel(10);
        let (rx_sender, rx_receiver) = tokio::sync::mpsc::channel(10);
        let retval = secure_concentrator::MessageStream {
            tx: tx_sender,
            rx: rx_receiver,
        };

        tokio::spawn(async move {
            let mut rx_frame = SpiFrame::new();
            let mut frame_num = rtp::SeqNum::start();

            loop {
                let mut tx_frame = SpiFrame::new();
                if let Ok(m) = tx_receiver.try_recv() {
                    if let Err(e) = tx_frame.set_msg(m) {
                        error!("{}", e);
                    }
                    tx_frame.header_0_set(rtp::Header(rtp::MsgType::ConfirmedDataDown, frame_num));
                    tx_frame.set_checksum(tx_frame.compute_checksum());
                }

                debug!("send tx_frame: {:?}", tx_frame);

                let mut transfer = SpidevTransfer::read_write(&tx_frame, &mut rx_frame);
                if let Err(e) = spi.transfer(&mut transfer) {
                    error!("error spi transfer: {}", e);
                }

                debug!("rx_frame: {:?}", rx_frame);

                match rx_frame.msg() {
                    Ok(None) => {}
                    Ok(Some(msg)) => {
                        if rx_frame.verify_checksum() {
                            rx_sender.send(msg).await.expect("can't send")
                        } else {
                            warn!("invalid checksum");
                        }
                    }

                    Err(e) => {
                        error!("rx_frame.msg(): {}", e);
                        std::process::exit(-1);
                    }
                }

                tokio::time::sleep(Duration::from_millis(1)).await;
                frame_num = frame_num.increment();
            }
        });

        Ok(retval)
    }
}
