use serde::Deserialize;

use crate::BoardConfig;

#[derive(Debug, Deserialize, PartialEq)]
pub struct GatewayConfig {
    pub local_address: String,
    pub server_address: String,

    #[serde(default = "default_keep_alive")]
    pub keep_alive: u32,
}

fn default_keep_alive() -> u32 {
    5
}

#[derive(Debug, Deserialize, PartialEq)]
pub struct RootConfig {
    pub gateway_conf: GatewayConfig,
    pub board: BoardConfig,
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_parse() {
        let str = r#"
        {
            "gateway_conf": {
                "local_address": "localhost:9000",
                "server_address": "localhost:1730",
                "keep_alive": 5
            },
            "board": {
                "reset_pin": 17,
                "com_type": "SPI_PROXY",
                "com_path": "/dev/spidev0.0",
                "clksrc": 0,
        
                "radio_0": {
                    "enable": true,
                    "type": "SX1250",
                    "freq": 904300000,
                    "rssi_offset": -215.4,
                    "rssi_tcomp": {"coeff_a": 0, "coeff_b": 0, "coeff_c": 20.41, "coeff_d": 2162.56, "coeff_e": 0},
                    "tx_enable": true,
                    "tx_freq_min": 923000000,
                    "tx_freq_max": 928000000,
                    "tx_gain_lut":[
                        {"rf_power": 12, "pa_gain": 0, "pwr_idx": 15},
                        {"rf_power": 13, "pa_gain": 0, "pwr_idx": 16},
                        {"rf_power": 14, "pa_gain": 0, "pwr_idx": 17},
                        {"rf_power": 15, "pa_gain": 0, "pwr_idx": 19},
                        {"rf_power": 16, "pa_gain": 0, "pwr_idx": 20},
                        {"rf_power": 17, "pa_gain": 0, "pwr_idx": 22},
                        {"rf_power": 18, "pa_gain": 1, "pwr_idx": 1},
                        {"rf_power": 19, "pa_gain": 1, "pwr_idx": 2},
                        {"rf_power": 20, "pa_gain": 1, "pwr_idx": 3},
                        {"rf_power": 21, "pa_gain": 1, "pwr_idx": 4},
                        {"rf_power": 22, "pa_gain": 1, "pwr_idx": 5},
                        {"rf_power": 23, "pa_gain": 1, "pwr_idx": 6},
                        {"rf_power": 24, "pa_gain": 1, "pwr_idx": 7},
                        {"rf_power": 25, "pa_gain": 1, "pwr_idx": 9},
                        {"rf_power": 26, "pa_gain": 1, "pwr_idx": 11},
                        {"rf_power": 27, "pa_gain": 1, "pwr_idx": 14}
                    ]
                },
                "radio_1": {
                    "enable": true,
                    "type": "SX1250",
                    "freq": 905000000,
                    "rssi_offset": -215.4,
                    "rssi_tcomp": {"coeff_a": 0, "coeff_b": 0, "coeff_c": 20.41, "coeff_d": 2162.56, "coeff_e": 0},
                    "tx_enable": false
                },
                "chan_multiSF_All": {"spreading_factor_enable": [ 5, 6, 7, 8, 9, 10, 11, 12 ]},
                "chan_multiSF_0": {"enable": true, "radio": 0, "if": -400000},  /* Freq : 903.9 MHz*/
                "chan_multiSF_1": {"enable": true, "radio": 0, "if": -200000},  /* Freq : 904.1 MHz*/
                "chan_multiSF_2": {"enable": true, "radio": 0, "if":  0},       /* Freq : 904.3 MHz*/
                "chan_multiSF_3": {"enable": true, "radio": 0, "if":  200000},  /* Freq : 904.5 MHz*/
                "chan_multiSF_4": {"enable": true, "radio": 1, "if": -300000},  /* Freq : 904.7 MHz*/
                "chan_multiSF_5": {"enable": true, "radio": 1, "if": -100000},  /* Freq : 904.9 MHz*/
                "chan_multiSF_6": {"enable": true, "radio": 1, "if":  100000},  /* Freq : 905.1 MHz*/
                "chan_multiSF_7": {"enable": true, "radio": 1, "if":  300000},  /* Freq : 905.3 MHz*/
                "chan_Lora_std":  {"enable": true, "radio": 0, "if":  300000, "bandwidth": 500000, "spread_factor": "SF8",                                              /* Freq : 904.6 MHz*/
                                   "implicit_hdr": false, "implicit_payload_length": 17, "implicit_crc_en": false, "implicit_coderate": 1},
                "chan_FSK":       {"enable": false, "radio": 1, "if":  300000, "bandwidth": 125000, "datarate": 50000}                                          /* Freq : 868.8 MHz*/
            }
        }
        "#;

        let reader = json_comments::StripComments::new(str.as_bytes());
        let _cfg: RootConfig = serde_json::from_reader(reader).unwrap();
    }
}
